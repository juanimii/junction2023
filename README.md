# Future Stars

Buenas, somos el equipo **rm -rf/***,

Imagina tener una idea en la que crees con pasión y quieras que otros también crean en ella. ¿Cómo lo harías? Nos encontramos ante el desafío FINTECH, donde debemos reunir a talentos ansiosos por aprender a emprender con empresas dispuestas a brindarles la oportunidad de crecer. Para abordar esta necesidad, hemos creado una plataforma de crowdfunding diseñada para financiar el camino de tantos jóvenes talentos como sea posible.

**¿Cuál es nuestra solución?** Todos compartimos la necesidad de innovar, mejorar y marcar la diferencia, y nosotros no somos una excepción. Hemos optado por ofrecer una solución que puede implementarse de inmediato: una PWA en lugar de una aplicación nativa. Esto nos permite llegar a un público más amplio a través de estrategias de SEO, SPIDERS e Internet, ofreciendo una experiencia completa sin necesidad de descargas, lo que aumenta significativamente la participación del público.

Esta solución beneficia a todos al brindar facilidad de uso, alcance, conexiones entre usuarios y la capacidad de utilizar el SEO para **llegar a públicos específicos**, como mujeres emprendedoras.

**¿Cómo funciona nuestra herramienta?** Comencemos con las **métricas de Lighthouse**: hemos dedicado especial atención al diseño para mejorar la usabilidad y la accesibilidad. Gracias a ello, hemos alcanzado una impresionante puntuación del 95% en la métrica de Lighthouse, todo esto en una demo que creamos en un solo fin de semana. ¡Espectacular, verdad?

Para todo el diseño web, hemos usado Angular 16, lo que permite a los desarrolladores trabajar de forma simultánea y apoyarse mutuamente sin caos. Si nos dan la oportunidad de seguir desarrollando este proyecto, hemos planteado implementar de forma factible las siguientes herramientas:

Una base de datos real, con datos reales donde implementemos correctamente diseño y perfiles de estudiantes y contribuidores.
Una IA en la nube, por ejemplo, GCP, la cual esté entrenada para valorar vídeos de los aspirantes al grado universitario basándose en los principios del proyecto (no será el único método de valoración), haciendo el proceso **más liviano para los reclutadores**.
**Estamos construyendo un puente entre estudiantes apasionados y empresas comprometidas con el cambio**. No hemos reinventado la rueda en términos de tecnología, pero hemos creado una solución innovadora que hace que conectar a personas sea fácil e inspirador.

¡Tenemos la solución! **¿Te unes a nosotros para descubrir estrellas ocultas?**



## Demo
Pruebalo aquí: <http://futurestars.hidlaz.duckdns.org/>

El registro tanto de estudiantes como inversores no está habilitado, aunque se mostrará la página de registro. Sin embargo, podrás probar el Login de ambos roles con las siguientes cuentas:

### Usuarios de prueba para estudiante:

- email: farahf@gmail.com
- pass:  123456

### Usuarios de prueba para inversores:

- email: chemi@gmail.com
- pass:  123456
