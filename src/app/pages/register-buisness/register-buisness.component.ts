import { Component, OnInit } from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Router } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-register-buisness',
  templateUrl: './register-buisness.component.html',
  styleUrls: ['./register-buisness.component.scss'],
  animations: [
    trigger('openClose', [
      state('true', style({ height: '*' })),
      state('false', style({ height: '0px' })),
      transition('false <=> true', [animate('0.20s ease')]),
    ]),
  ],
})
export class RegisterBuisnessComponent {
  preguntas = [] as any;
  isStep2 = false;

  constructor(private router: Router, public sanitizer: DomSanitizer) {}
  send() {
    this.router.navigate(['/register-success']);
  }
}
