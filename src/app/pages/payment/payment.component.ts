import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { timer } from 'rxjs';
import { AccountService } from 'src/app/services/account.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit {
  amount: number = null as any;
  isLoading = false;

  constructor(private router: Router, private accountService: AccountService) {}

  ngOnInit(): void {}

  done() {
    if (this.amount === null) {
      return;
    }
    this.isLoading = true;
    timer(2500).subscribe(() => {
      this.accountService.saveMoney(+this.amount);
      this.router.navigate(['/home-buisness']);
    });
  }
}
